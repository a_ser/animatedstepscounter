package com.example.andrii.simplechart.views;

import android.animation.ArgbEvaluator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Property;

import com.example.andrii.simplechart.R;

/**
 * Created by andrii on 29/01/2016.
 */
public class DashedCircleSteps extends OuterCircleView {
    private static final String TAG = DashedCircleSteps.class.getSimpleName();

    /**
     * Radius of circle with dashes
     */
    private int mInnerCircleRadius;


    /**
     * Paint for inner circle (with dashes) in case of active steps
     */
    private Paint mInnerActivePaint;
    /**
     * Paint for inactive dashes
     */
    private Paint mInnerNonActivePaint;


    /**
     * Step's limit we want reach. If we reach it every dash will be active. Otherwise difference
     * will be colored with {@link #mInnerNonActivePaint}
     */
    private int mMaxDaySteps = 8000;
    private int mCurrentSteps = 4000;
    /**
     * How many dashes we wanna draw
     */
    private static final int LINES_COUNT = 180;
    /**
     * The length of dashes
     */
    private static final int LINE_LENGTH = 30;
    /**
     * Evaluator for smoothly changes of active color
     */
    private ArgbEvaluator mArgbEvaluator = new ArgbEvaluator();
    private int mMaxActiveColor = 0x5eb7e0;
    private int mMinActiveColor = 0xde5d86;

    public DashedCircleSteps(Context context) {
        super(context);
        init(null);
    }

    public DashedCircleSteps(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public DashedCircleSteps(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DashedCircleSteps(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(AttributeSet set) {
        if (set != null) {
            TypedArray res = getContext().obtainStyledAttributes(set, R.styleable.DashedCircleSteps);
            mMaxActiveColor = res.getColor(R.styleable.DashedCircleSteps_active_color_min, mMaxActiveColor);
            mMinActiveColor = res.getColor(R.styleable.DashedCircleSteps_active_color_max, mMinActiveColor);
            res.recycle();
        }

        mInnerActivePaint = new Paint();
        mInnerActivePaint.setAntiAlias(true);
        mInnerActivePaint.setStrokeWidth(2);

        mInnerNonActivePaint = new Paint();
        mInnerNonActivePaint.setAntiAlias(true);
        mInnerNonActivePaint.setColor(mInactiveColor);
        mInnerNonActivePaint.setStrokeWidth(2);

    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mInnerCircleRadius = mOuterCirceRadius - 80;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawStepLines(canvas);
    }

    private void drawStepLines(Canvas canvas) {
        int activeCount = LINES_COUNT * mCurrentSteps / mMaxDaySteps;
        int cx, cy;

        float angleByLine = 360f / LINES_COUNT;
        // center of circle
        cx = cy = getWidth() / 2;
        for (int i = 0; i < LINES_COUNT; i++) {
            float inner[] = getCoordinates(cx, cy, angleByLine, i, mInnerCircleRadius);
            float out[] = getCoordinates(cx, cy, angleByLine, i, mInnerCircleRadius + LINE_LENGTH);
            if (i < activeCount) {
                updateInnerActivePaint(i);
                mTempCanvas.drawLine(inner[0], inner[1], out[0], out[1], mInnerActivePaint);
            } else {
                mTempCanvas.drawLine(inner[0], inner[1], out[0], out[1], mInnerNonActivePaint);
            }
        }
        canvas.drawBitmap(mTempBitmap, 0, 0, null);
    }

    /**
     * Return coordinates on circle
     *
     * @param cx          x coordinates of circle center
     * @param cy          y coordinates of circle center
     * @param angleByLine angle in degree angel
     * @param i           number of line
     * @param radius      of circle
     * @return x, y coordinates of dot on circle
     */
    private float[] getCoordinates(int cx, int cy, float angleByLine, int i, int radius) {
        float x = (cx + (radius * (float) Math.cos((angleByLine * i - 90) * Math.PI / 180)));
        float y = (cy + (radius * (float) Math.sin((angleByLine * i - 90) * Math.PI / 180)));
        return new float[]{x, y};
    }

    private void updateInnerActivePaint(int i) {
        double progress = i / (double) LINES_COUNT;
        mInnerActivePaint.setColor((Integer) mArgbEvaluator.evaluate((float) progress, mMaxActiveColor, mMinActiveColor));
    }

    public void setCurrentSteps(int currentSteps) {
        mCurrentSteps = currentSteps;
        postInvalidate();
    }

    public int getCurrentSteps() {
        return mCurrentSteps;
    }

    public static final Property<DashedCircleSteps, Integer> STEPS_PROGRESS = new Property<DashedCircleSteps, Integer>(Integer.class, "stepProgress") {

        @Override
        public Integer get(DashedCircleSteps object) {
            return object.getCurrentSteps();
        }

        @Override
        public void set(DashedCircleSteps object, Integer value) {
            object.setCurrentSteps(value);
        }
    };
}
