package com.example.andrii.simplechart;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.andrii.simplechart.views.StepsCounterView;

public class MainActivity extends AppCompatActivity {
    private StepsCounterView mStepsCounterView;
    private int mCurrentSteps = 4873;
    private boolean isAnimationShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkSavedInstanceState(savedInstanceState);
        mStepsCounterView = (StepsCounterView) findViewById(R.id.steps_circle_view);
        mStepsCounterView.setCurrentSteps(mCurrentSteps);
        mStepsCounterView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mStepsCounterView.playClickAnimation();
            }
        });
        ((TextView) findViewById(R.id.steps_view)).setText(String.valueOf(mCurrentSteps));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isAnimationShown) {
            mStepsCounterView.playAnimation();
            isAnimationShown = true;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("IS_ANIMATION_PLAYED", isAnimationShown);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        checkSavedInstanceState(savedInstanceState);
    }

    private void checkSavedInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState == null) return;

        isAnimationShown = savedInstanceState.getBoolean("IS_ANIMATION_PLAYED");
    }
}
