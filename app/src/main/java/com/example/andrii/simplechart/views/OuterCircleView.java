package com.example.andrii.simplechart.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Property;
import android.view.View;

import com.example.andrii.simplechart.R;

/**
 * Created by andrii on 31/01/2016.
 */
public class OuterCircleView extends View {
    private static final String TAG = OuterCircleView.class.getSimpleName();
    /**
     * We will add appearing animation and enlarge radius of circle
     */
    public static int INIT_CIRCLE_PADDING = 35;
    /**
     * The biggest circle radius
     */
    protected int mOuterCirceRadius, mOuterCirceRadiusTmp;
    /**
     * Paints for biggest circle (gray one) and invisible paint which clear inside of this circle and
     * leaves only thin border
     */
    protected Paint mOuterCirclePaint;
    protected Paint mOuterCirclePaint1;
    /**
     * Temp stuff for truly transparency
     */
    protected Bitmap mTempBitmap;
    protected Canvas mTempCanvas;
    protected int mInactiveColor;

    public OuterCircleView(Context context) {
        super(context);
        init(null);
    }

    public OuterCircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public OuterCircleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public OuterCircleView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(AttributeSet set) {
        mInactiveColor = Color.parseColor("#5eb7e0");
        if (set != null) {
            TypedArray res = getContext().obtainStyledAttributes(set, R.styleable.DashedCircleSteps);
            mInactiveColor = res.getColor(R.styleable.DashedCircleSteps_inactive_color, mInactiveColor);
            res.recycle();
        }
        mOuterCirclePaint = new Paint();
        mOuterCirclePaint.setColor(mInactiveColor);
        mOuterCirclePaint.setAntiAlias(true);

        mOuterCirclePaint1 = new Paint();
        mOuterCirclePaint1.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        mOuterCirclePaint1.setAntiAlias(true);
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mOuterCirceRadiusTmp = mOuterCirceRadius = w / 2;
        mOuterCirceRadiusTmp += OuterCircleView.INIT_CIRCLE_PADDING - 5;
        mTempBitmap = Bitmap.createBitmap(getWidth(), getWidth(), Bitmap.Config.ARGB_8888);
        mTempCanvas = new Canvas(mTempBitmap);
    }

    public void reset() {
        mOuterCirceRadiusTmp = mOuterCirceRadius;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Log.d(TAG, "onDraw");
        mTempCanvas.drawColor(0xffffff, PorterDuff.Mode.CLEAR);
        drawOuterCircle(canvas);
    }

    private void drawOuterCircle(Canvas canvas) {
        int size = getWidth() / 2;
        mTempCanvas.drawCircle(size, size, mOuterCirceRadiusTmp - INIT_CIRCLE_PADDING, mOuterCirclePaint);
        // Erase center of circle
        float radiusDifference = 3;
        mTempCanvas.drawCircle(size, size, mOuterCirceRadiusTmp - INIT_CIRCLE_PADDING - radiusDifference, mOuterCirclePaint1);
        canvas.drawBitmap(mTempBitmap, 0, 0, null);
    }

    public void setOuterCirceRadius(int outerCirceRadius) {
        mOuterCirceRadiusTmp = outerCirceRadius;
        postInvalidate();
    }

    public int getOuterCirceRadius() {
        return mOuterCirceRadius;
    }

    public int getOuterCirceRadiusTmp() {
        return mOuterCirceRadiusTmp;
    }

    public static Property<OuterCircleView, Integer> OUTER_CIRCLE = new Property<OuterCircleView, Integer>(Integer.class, "outerCircleRadius") {
        @Override
        public Integer get(OuterCircleView object) {
            return object.getOuterCirceRadius();
        }

        @Override
        public void set(OuterCircleView object, Integer value) {
            object.setOuterCirceRadius(value);
        }
    };


}
