package com.example.andrii.simplechart.views;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ViewTreeObserver;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;

/**
 * Created by andrii on 31/01/2016.
 */
public class StepsCounterView extends FrameLayout {
    private static final String TAG = StepsCounterView.class.getSimpleName();
    private TimeInterpolator mInterpolator = new OvershootInterpolator();
    private AnimatorSet mAnimatorSet;
    private int mCurrentSteps;
    private DashedCircleSteps mDashedCircleSteps;
    private OuterCircleView mOuterCircleView;

    public StepsCounterView(Context context) {
        super(context);
        init(null);
    }

    public StepsCounterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public StepsCounterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public StepsCounterView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        mDashedCircleSteps = new DashedCircleSteps(getContext(), attrs);
        mOuterCircleView = new OuterCircleView(getContext(), attrs);
        addView(mOuterCircleView);
        addView(mDashedCircleSteps);
        mOuterCircleView.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
    }

    public void setCurrentSteps(int currentSteps) {
        mCurrentSteps = currentSteps;
        mDashedCircleSteps.setCurrentSteps(mCurrentSteps);
    }

    public void playAnimation() {
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {

                // Removing layout listener to avoid multiple calls
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                playClickAnimation();
            }
        });

    }

    public void playClickAnimation() {
        Log.d(TAG, "playAnimation");
        if (mAnimatorSet != null) {
            mAnimatorSet.cancel();
        }
        mAnimatorSet = new AnimatorSet();
        ObjectAnimator innerCircleAnimator = ObjectAnimator.ofInt(mDashedCircleSteps, DashedCircleSteps.STEPS_PROGRESS, 0, mCurrentSteps);
        innerCircleAnimator.setDuration(1450);
        innerCircleAnimator.setInterpolator(mInterpolator);

        mOuterCircleView.reset();
        ObjectAnimator outerCircleRadiusAnimator = ObjectAnimator.ofInt(mOuterCircleView, OuterCircleView.OUTER_CIRCLE, mOuterCircleView.getOuterCirceRadiusTmp(), mOuterCircleView.getOuterCirceRadiusTmp() + OuterCircleView.INIT_CIRCLE_PADDING - 5);
        outerCircleRadiusAnimator.setDuration(750);
        outerCircleRadiusAnimator.setInterpolator(new AnticipateOvershootInterpolator());

        mAnimatorSet.playTogether(innerCircleAnimator, outerCircleRadiusAnimator);
        mAnimatorSet.start();
    }
}
